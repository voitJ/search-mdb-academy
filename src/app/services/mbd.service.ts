import { Injectable, OnInit } from '@angular/core';
import * as TheMoviedb from 'themoviedb-javascript-library';


const apiKey = "ae5dad6a28571e1a27b493de2e9607f3";
@Injectable()
export class MbdService {
  mdbApi : TheMoviedb;  
  constructor() {
    this.mdbApi = TheMoviedb;
    this.mdbApi.common.api_key = apiKey;
    this.mdbApi.common.timeout = 2000
   }

  getMbdService() {
    return this.mdbApi;
   }
}



