import { TestBed, inject } from '@angular/core/testing';
import { MbdService } from './mbd.service';

describe('MbdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MbdService]
    });
  });

  it('should be created', inject([MbdService], (service: MbdService) => {
    expect(service).toBeTruthy();
  }));
});
