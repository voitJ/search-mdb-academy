export interface Title {
    vote_average : number;
    id : number;
    title : string;
    popularity : number;
    genre_ids : string;
    release_date : string;
    poster_path : string;
    overview : string;  
}
