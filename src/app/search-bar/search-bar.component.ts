import { Component, OnInit, ViewChild } from '@angular/core';
import { MbdService } from '../services/mbd.service';
import * as TheMoviedb from 'themoviedb-javascript-library';
import { Title } from '../model/title';
import { MatAutocompleteTrigger } from '@angular/material';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})

export class SearchBarComponent implements OnInit {

  @ViewChild( MatAutocompleteTrigger)
  autoTrigger: MatAutocompleteTrigger;
  
  strSearch : string;

  //title typed on searchBar
  titlesToSearch : Array<Title> = null;

  //title entered on searchBar
  titleSearched : Array<Title> = null;

  mdbApi : TheMoviedb;
  showErr : boolean = false;
  showDetail : boolean = false;
  
  constructor(public mbdService : MbdService) {
    this.mdbApi = mbdService.getMbdService();
   }

   ngOnInit() {
  }

  onTypeText() {
    if (this.strSearch === "") {
      this.titlesToSearch = null;
    }else {
      this.showErr = false;
      this.searchFilmFiltered(this.strSearch);
    }
  }

  onEnterText(auto: any) {
    this.autoTrigger.closePanel();
    
    if (this.strSearch == null || this.strSearch.trim() === "") {
      this.showErr = true;
    }else {
      this.showErr = false;
      this.searchFilm(this.strSearch);
    }
  }

  private errorCB(data) : void {
    console.log("Error callback: " + data);
  }

  public searchFilmFiltered(strToSearch : string) : void {
    let strEncoded = encodeURI(strToSearch);
    this.mdbApi.search.getMovie({"query":strEncoded}, data => {
      this.titlesToSearch = JSON.parse(data).results.filter(t => {
        return t.title.toLowerCase().indexOf(strToSearch.toLowerCase()) == 0;
      });
    },this.errorCB);
  }

  public searchFilm(strToSearch : string) : void {
    let strEncoded = encodeURI(strToSearch);
    this.mdbApi.search.getMovie({"query":strEncoded}, data => {
      this.titleSearched = JSON.parse(data).results;
    },this.errorCB);
  }

  public clickOnList(title : Title) {
    this.titleSearched = new Array<Title>();
    this.titleSearched.push(title);
  }
}
