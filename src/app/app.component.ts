import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  @HostBinding('style.background-color')    
  bgColor = '#009688';
}
